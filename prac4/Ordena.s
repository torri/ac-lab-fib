 .text
	.align 4
	.globl Ordenar
	.type Ordenar,@function
Ordenar:
        # Aqui viene vuestro codigo
	
	#classic
	pushl %ebp
	movl %esp,%ebp
	
	#locals
	subl	 $8,%esp
	pushl	 %ebx
	pushl	 %edi
	pushl	 %esi

	movl	 $0 , %esi	#i = 0
				#j <-> -8(%ebp)
	movl 	 8(%ebp),%ecx 	# @v
fori: 	
	imull	 $12,%esi,%ebx		# i * 12
	cmpl	 $0x80000000,4(%ebx,%ecx)	# v[i].k != 0x8....
	je 	 endfori

	movl	 $1, %edi
	addl	 %esi,%edi			#j = i+1

forj:
	imull	 $12,%edi,%edx		# j * 12
	cmpl	 $0x80000000,4(%ecx,%edx)	# v[j].k != 0x8....
	je 	 endforj
	
	movl	 4(%ebx,%ecx),%eax		#v[i].k
	cmpl	 4(%ecx,%edx),%eax		#v[j].k < v[i].k ? 
	jle	 endif

	#entra al if
	pushl 	%eax
	pushl 	%ecx
	pushl	%edx

	pushl	%edi			#param j
	pushl	%esi			#param i
	pushl	%ecx				#param v

	
	call    Intercambiar
	
	#volvimo	
	addl    $12,%esp

	popl	%edx
	popl	%ecx
	popl	%eax

endif:	
	incl	%edi			#j++
	jmp 	forj

endforj:
	incl 	%esi			#i++
	jmp 	fori

endfori:

	movl	%esi,%eax			#pel return
	
	#fi :(
	popl	%esi
	popl	%edi
	popl	%ebx

	movl	%esp,%ebp
	popl	%ebp
	ret
	
