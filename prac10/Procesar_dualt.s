.text
	.align 4
	.globl procesar
	.type	procesar, @function
procesar:
	pushl	%ebp
	movl	%esp, %ebp
	subl	$16, %esp
	pushl	%ebx
	pushl	%esi
	pushl	%edi

# Aqui has de introducir el codigo
    movl 8(%ebp), %ebx
    movl 12(%ebp), %esi
    movl 16(%ebp), %ecx
    imul %ecx, %ecx #n*n
    
    leal 12(%ebp), %eax
    test $0x0000000F, %eax
    jne unaligned
    leal 8(%ebp), %eax
    test $0x0000000F, %eax
    jne unaligned
    
    movl $0, %eax
foraligned: 
    cmpl %ecx, %eax
    jge end
    movdqa (%ebx, %eax), %xmm0
    psllq $4, %xmm0
    movdqa %xmm0, (%esi, %eax)
    addl $16, %eax
    jmp foraligned

unaligned: 
    movl $0, %eax
forunaligned: cmpl %ecx, %eax
    jge end
    movdqu (%ebx, %eax), %xmm0
    psllq $4, %xmm0
    movdqu %xmm0, (%esi, %eax)
    addl $16, %eax
    jmp forunaligned
    
end:

# El final de la rutina ya esta programado

	emms	# Instruccion necesaria si os equivocais y usais MMX
	popl	%edi
	popl	%esi
	popl	%ebx
	movl %ebp,%esp
	popl %ebp
	ret
