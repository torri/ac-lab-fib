#include "CacheSim.h"

/* Posa aqui les teves estructures de dades globals
 * per mantenir la informacio necesaria de la cache
 * */


struct cachee{
	unsigned int tagc; 
	unsigned int dirty;
	unsigned int bitv;
};

struct cachee cache[128];


/* La rutina init_cache es cridada pel programa principal per
 * inicialitzar la cache.
 * La cache es inicialitzada al comen�ar cada un dels tests.
 * */
void init_cache ()
{
	/* Escriu aqui el teu codi */
	for (int i = 0; i < 128; i++){ 
		cache[i].dirty = 0;
		cache[i].bitv  = 0; 
	}


}

/* La rutina reference es cridada per cada referencia a simular */ 
void reference (unsigned int address, unsigned int LE)
{
	unsigned int byte;
	unsigned int bloque_m; 
	unsigned int linea_mc;
	unsigned int tag;
	unsigned int miss;
	unsigned int lec_mp;
	unsigned int mida_lec_mp;
	unsigned int esc_mp;
	unsigned int mida_esc_mp;
	unsigned int replacement;
	unsigned int tag_out;

	/* Escriu aqui el teu codi */

	/*  cache de 4kb i l�nees de 32 bits    */
	/*  4kb / 32 bits = 128 l�nees  */
	/*  32 bits per l�nea -> neccessitem 5 bits del offser  */
	/*  128 l�nees -> necessitem 7 bits per representar la l�nea */ 

	//el byte s�n els �ltims 5 bits, els treiem amb una m�scara de 0b0001 1111 (0d31)
	byte     = address&31;                                                            
	//el bloc �s l'adre�a menys els �ltims 5 bits de byte
	bloque_m = address>>5;
	//la l�nea de mc s�n els bits del 6 al 12 (ambd�s inclosos), fem shift i la m�scar    a 
	linea_mc = (address>>5)&127;
	//el tag �s tota la resta (bits del 13 al 32 ambd�s inclosos)
	tag = address>>12;

	if (LE == 0){			//lectura
		//mirar hit or miss
		if(tag == cache[linea_mc].tagc && cache[linea_mc].bitv == 1){ //hit
			miss = 0; 
			lec_mp = 0; 
			mida_lec_mp = 0;
			esc_mp = 0; 
			mida_esc_mp = 0;
			replacement = 0; 

		} else { //miss
			miss = 1; 
			lec_mp = 1; 
			mida_lec_mp = 32;

			if(cache[linea_mc].bitv == 1){
			//v�lida
				if(cache[linea_mc].dirty == 1){
					esc_mp = 1; 
					mida_esc_mp = 32;

				} else {
					esc_mp = 0;
					mida_esc_mp = 0; 
				}
				replacement = 1; 
				tag_out = cache[linea_mc].tagc;
			} else {
			//no v�lida
	
				replacement = 0; 
				esc_mp = 0; 
				mida_esc_mp = 0;
			}
			
			cache[linea_mc].tagc = tag; 
			cache[linea_mc].dirty = 0;
			cache[linea_mc].bitv =  1;
		}
	
	} else {			//escritura
		//mirar hit or miss
		if(tag == cache[linea_mc].tagc && cache[linea_mc].bitv == 1){ //hit
			miss = 0; 
			lec_mp = 0; 
			mida_lec_mp = 0;
			esc_mp = 0; 
			mida_esc_mp = 0; 
			replacement = 0;
			cache[linea_mc].dirty = 1; 			
		
		} else {
		
			miss = 1; 
			lec_mp = 1; 
			mida_lec_mp = 32;

			if(cache[linea_mc].bitv == 1){
			//v�lida
			
				if(cache[linea_mc].dirty == 1){
					esc_mp = 1; 
					mida_esc_mp = 32;

				} else {
					esc_mp = 0;
					mida_esc_mp = 0;
				}

				replacement = 1; 
				tag_out = cache[linea_mc].tagc;
			
			} else {
			//no v�lida
				replacement = 0; 
				esc_mp = 0; 
				mida_esc_mp = 0;
			}
			
			cache[linea_mc].tagc = tag; 
			cache[linea_mc].dirty = 1;
			cache[linea_mc].bitv = 1;
		}	
	
	}
	/* La funcio test_and_print escriu el resultat de la teva simulacio
	 * per pantalla (si s'escau) i comproba si hi ha algun error
	 * per la referencia actual
	 * */
	test_and_print (address, LE, byte, bloque_m, linea_mc, tag,
			miss, lec_mp, mida_lec_mp, esc_mp, mida_esc_mp,
			replacement, tag_out);

}


/* La rutina final es cridada al final de la simulacio */ 
void final ()
{
 	/* Escriu aqui el teu codi */ 
  
  
}
