.text
	.align 4
	.globl OperaMat
	.type	OperaMat, @function
OperaMat:
	pushl	%ebp
	movl	%esp, %ebp
	subl	$16, %esp
	pushl	%ebx
	pushl	%esi
	pushl	%edi
# Aqui has de introducir el codigo
	movl	$0,-4(%ebp)  	#res = 0  
	movl 	$0,-8(%ebp) 	#i = 0
fori: 	
	cmpl   	$3,-8(%ebp)	#i<3?
	jge	fi		#s'acaba si false
	movl	$0,-12(%ebp)	#j = 0			
forj:
	cmpl	$3,-12(%ebp)	#j<3?
	jge 	fiforj		#s'acaba aquest bucle si false
	
	imull 	$3,-12(%ebp),%ebx	# ebx = i*N
	addl	-8(%ebp),%ebx		# ebx = (i*N)+j
	movl	8(%ebp),%ecx 		# ecx = @mat
	movl	(%ecx,%ebx,4),%ecx	# ecx = Mat[i][j]
	
	addl	%ecx,-4(%ebp)		# res += Mat[i][j]
	
	movl	12(%ebp),%ecx		#ecx = salto
	addl	%ecx,-12(%ebp)		#j += salto
	jmp	forj
 
fiforj:
	movl	12(%ebp),%ecx		#ecx = salto
	addl	%ecx,-8(%ebp)		#i += salto
	jmp	fori

fi:
 
# El final de la rutina ya esta programado
	movl	-4(%ebp), %eax
	popl	%edi
	popl	%esi
	popl	%ebx	
	movl %ebp,%esp
	popl %ebp
	ret
