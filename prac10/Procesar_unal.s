.text
	.align 4
	.globl procesar
	.type	procesar, @function
procesar:
	pushl	%ebp
	movl	%esp, %ebp
	subl	$16, %esp
	pushl	%ebx
	pushl	%esi
	pushl	%edi

# Aqui has de introducir el codigo
	movl 	8(%ebp),%esi
	movl	12(%ebp),%edi
	movl 	$0,%ebx
for: 
	cmpl	$10000,%ebx
	jge 	fifor
	movdqu	(%esi,%ebx),%xmm0 #xmm0 <- n
	paddb	%xmm0,%xmm0	#2n
	paddb	%xmm0,%xmm0	#4n
	paddb	%xmm0,%xmm0	#8n
	paddb	%xmm0,%xmm0	#16n
	movdqu	%xmm0,(%edi,%ebx) #16n->aha
	addl 	$16,%ebx
	jmp 	for

fifor:
# El final de la rutina ya esta programado

	emms	# Instruccion necesaria si os equivocais y usais MMX
	popl	%edi
	popl	%esi
	popl	%ebx
	movl %ebp,%esp
	popl %ebp
	ret
