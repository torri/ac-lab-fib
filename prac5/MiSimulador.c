#include "CacheSim.h"
#include <string.h> 
#include <stdio.h>

/* Posa aqui les teves estructures de dades globals
 * per mantenir la informacio necesaria de la cache
 * */


//�s una cache directa i per tant nom�s necessitarem mirar si el tag �s el mateix
//tenim 128 l�nees per tant un tag per l�nea
//hem decidit representar-ho com un vector on es guarda el tag a cada posici�
unsigned int cache[128];
unsigned int hits; 
unsigned int misses; 
/* La rutina init_cache es cridada pel programa principal per
 * inicialitzar la cache.
 * La cache es inicialitzada al comen�ar cada un dels tests.
 * */
void init_cache ()
{
    totaltime=0.0;
	/* Escriu aqui el teu codi */
	/*posem tots els tags al nombre m�s gran que hi cap a un unsigned int per inicialitzar la cache*/
    	for (int i = 0; i < 128; i++) cache[i] = 1410065407; 

	hits = 0; 
	misses = 0; 
}

/* La rutina reference es cridada per cada referencia a simular */ 
void reference (unsigned int address)
{
	unsigned int byte;
	unsigned int bloque_m; 
	unsigned int linea_mc;
	unsigned int tag;
	unsigned int miss;	   // boolea que ens indica si es miss
	unsigned int replacement;  // boolea que indica si es reempla�a una linia valida
	unsigned int tag_out;	   // TAG de la linia reempla�ada
	float t1,t2;		// Variables per mesurar el temps (NO modificar)
	
	t1=GetTime();
	/* Escriu aqui el teu codi */

	/*  cache de 4kb i l�nees de 32 bits    */
	/*  4kb / 32 bits = 128 l�nees 	*/
	/*  32 bits per l�nea -> neccessitem 5 bits del offser	*/
	/*  128 l�nees -> necessitem 7 bits per representar la l�nea */ 

	//el byte s�n els �ltims 5 bits, els treiem amb una m�scara de 0b0001 1111 (0d31)
	byte 	 = address&31;
	//el bloc �s l'adre�a menys els �ltims 5 bits de byte
	bloque_m = address>>5;
	//la l�nea de mc s�n els bits del 6 al 12 (ambd�s inclosos), fem shift i la m�scara 
	linea_mc = (address>>5)&127;
	//el tag �s tota la resta (bits del 13 al 32 ambd�s inclosos)
	tag = address>>12; 

	//mirar si �s hit or miss
	
	if (tag == cache[linea_mc]){ // hit
		miss = 0; 		
		replacement = 0;
		tag_out = 1; //simb�lic, per indicar que no s'ha reempla�at res 
		
		hits++;

		} else {		    // miss
			//pas 1:  mirar quina l�nea s'ha de reempla�ar
			//i si porta una direcci� "v�lida"
			//suposem que si porta un 1410065407 al tag, no �s v�lida
			miss = 1;
		
			if(cache[linea_mc] == 1410065407){ //l�nea no v�lida
				replacement = 0; 
			} else {
				replacement = 1; 
				tag_out = cache[linea_mc];
			}
		
		//pas 2: fer reempla�ament
		cache[linea_mc] = tag; 
		misses++; 
			
	}


	/* La funcio test_and_print escriu el resultat de la teva simulacio
	 * per pantalla (si s'escau) i comproba si hi ha algun error
	 * per la referencia actual. Tamb� mesurem el temps d'execuci�
	 * */
	t2=GetTime();
	totaltime+=t2-t1;
	test_and_print (address, byte, bloque_m, linea_mc, tag,
			miss, replacement, tag_out);
}

/* La rutina final es cridada al final de la simulacio */ 
void final ()
{
 	/* Escriu aqui el teu codi */ 
  	char buffer[128];

	sprintf(buffer,"hits: %d\n",hits); 
	write(1,buffer,strlen(buffer)); 
	
	sprintf(buffer,"misses: %d\n",misses); 
	write(1,buffer,strlen(buffer)); 
  
}
